(function ($) {

Drupal.fullcalendar.plugins.fullcalendar_create = {
  options: function (fullcalendar, settings) {
    if (!settings.fullcalendar_create) {
      return;
    }

    Drupal.ajax.prototype.commands.fullcalendar_create_reload = function (ajax, data, status) {
      location.reload();
    };

    var options = {};
    if (settings.fullcalendar_create.select) {
      options.selectable = true;
      options.select = function (startDate, endDate, allDay, jsEvent, view) {
        var ajax = new Drupal.ajax('main', fullcalendar.$calendar[0], {
          event: 'fullcalendar_create_add_select',
          url: '/fullcalendar_create/ajax/add/' + settings.fullcalendar_create.node_type,
          submit: {
            fullcalendar_create_start_date: $.fullCalendar.formatDate(startDate, 'u'),
            fullcalendar_create_end_date: $.fullCalendar.formatDate(endDate, 'u'),
            fullcalendar_create_date_field: settings.fullcalendar_create.date_field
          }
        });
        $(ajax.element)
          .bind('fullcalendar_create_add_select', Drupal.CTools.Modal.clickAjaxLink)
          .trigger('fullcalendar_create_add_select');
        this.unselect();
      };
    }
    if (settings.fullcalendar_create.click) {
      options.dayClick = function (date, allDay, jsEvent, view) {
        var ajax = new Drupal.ajax('main', fullcalendar.$calendar[0], {
          event: 'fullcalendar_create_add_click',
          url: '/fullcalendar_create/ajax/add/' + settings.fullcalendar_create.node_type,
          submit: {
            fullcalendar_create_start_date: $.fullCalendar.formatDate(date, 'u'),
            fullcalendar_create_date_field: settings.fullcalendar_create.date_field
          }
        });
        $(ajax.element)
          .bind('fullcalendar_create_add_click', Drupal.CTools.Modal.clickAjaxLink)
          .trigger('fullcalendar_create_add_click');
      };
    }
    return options;
  }
};

Drupal.fullcalendar.plugins.fullcalendar_edit = {
  options: function (fullcalendar, settings) {
    if (!settings.fullcalendar_edit) {
      return;
    }

    Drupal.ajax.prototype.commands.fullcalendar_edit_reload = function (ajax, data, status) {
      location.reload();
    };

    var options = {};
    if (settings.fullcalendar_edit.click) {
      options.eventClick = function (event, jsEvent, view) {

        var ajax = new Drupal.ajax('main', $(this), {
          event: 'fullcalendar_edit_add_click',
          url: '/fullcalendar_edit/ajax/sessions/edit/' + event.eid,
          submit: {
            fullcalendar_edit_revisions: settings.fullcalendar_edit.revisions,
            fullcalendar_edit_authoring: settings.fullcalendar_edit.authoring,
            fullcalendar_edit_publishing: settings.fullcalendar_edit.publishing
          }
        });
        jsEvent.preventDefault();
        jsEvent.stopPropagation();
        $(ajax.element)
          .bind('fullcalendar_edit_add_click', Drupal.CTools.Modal.clickAjaxLink)
          .trigger('fullcalendar_edit_add_click');
      };
    }
    return options;
  }
};

}(jQuery));
