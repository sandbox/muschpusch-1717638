<?php

/**
 * @file
 * Provides extra FullCalendar configuration options.
 */

/**
 * Implements hook_fullcalendar_options_info().
 */
function fullcalendar_edit_fullcalendar_options_info() {
  return array(
    'fullcalendar_edit' => array(
      'name' => 'FullCalendar edit',
      'js' => TRUE,
      'css' => TRUE,
    ),
    'fullcalendar_create' => array(
      'name' => 'FullCalendar Create',
      'js' => TRUE,
      'css' => TRUE,
    ),

  );
}

/**
 * Implements hook_fullcalendar_options_definition().
 */
function fullcalendar_edit_fullcalendar_options_definition() {

  $options['fullcalendar_edit']['contains'] = array(
    'edit_click' => array(
      'default' => FALSE,
      'bool' => TRUE,
    ),
    'edit_revisions' => array(
      'default' => FALSE,
      'bool' => TRUE,
    ),
    'edit_authoring' => array(
      'default' => FALSE,
      'bool' => TRUE,
    ),
    'edit_publishing' => array(
      'default' => FALSE,
      'bool' => TRUE,
    ),
    'create_click' => array(
        'default' => FALSE,
        'bool' => TRUE,
      ),
      'create_select' => array(
        'default' => FALSE,
        'bool' => TRUE,
      ),
      'create_node_type' => array('default' => ''),
      'create_date_field' => array('default' => array()),
    
    'create_revisions' => array(
      'default' => FALSE,
      'bool' => TRUE,
    ),
    'create_authoring' => array(
      'default' => FALSE,
      'bool' => TRUE,
    ),
    'create_publishing' => array(
      'default' => FALSE,
      'bool' => TRUE,
    ),
  );

/*
    $options['fullcalendar_create']['contains'] = array(
      'click' => array(
        'default' => FALSE,
        'bool' => TRUE,
      ),
      'select' => array(
        'default' => FALSE,
        'bool' => TRUE,
      ),
      'node_type' => array('default' => ''),
      'date_field' => array('default' => array()),
    
    'revisions2' => array(
      'default' => FALSE,
      'bool' => TRUE,
    ),
    'authoring' => array(
      'default' => FALSE,
      'bool' => TRUE,
    ),
    'publishing' => array(
      'default' => FALSE,
      'bool' => TRUE,
    ),

  );
  */

  return $options;
}

/**
 * Implements hook_fullcalendar_options_form().
 * TODO: this form is a mess refactor it!
 */
function fullcalendar_edit_fullcalendar_options_form(&$form, &$form_state, &$view) {
  $node_type = node_type_get_names();
  $bundles = array();
  foreach ($view->view->field as $field_name => $field) {
    if (fullcalendar_field_is_date($field)) {
      foreach ($field->field_info['bundles']['node'] as $bundle) {
        $bundles[$bundle] = $node_type[$bundle];
      }
    }
  }
  
  //create
  $form['fullcalendar_edit']['create_click'] = array(
    '#type' => 'checkbox',
    '#prefix' => t('Create options:'),
    '#title' => t('Add new events when clicking on a day'),
    '#default_value' => $view->options['fullcalendar_edit']['create_click'],
    '#data_type' => 'bool',
  );

  $form['fullcalendar_edit']['create_select'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add new events when selecting a day'),
    '#default_value' => $view->options['fullcalendar_edit']['create_select'],
    '#data_type' => 'bool',
  );

  $form['fullcalendar_edit']['create_node_type'] = array(
    '#type' => 'select',
    '#title' => t('Select the node type to prepopulate'),
    '#options' => $bundles,
    '#default_value' => $view->options['fullcalendar_edit']['create_node_type'],
    '#dependency' => array(
      'edit-style-options-fullcalendar-create-click' => array(1),
      'edit-style-options-fullcalendar-create-select' => array(1),
    ),
  );

  $form['fullcalendar_edit']['create_date_field'] = array(
    '#type' => 'select',
    '#title' => t('Select the date field to prepopulate'),
    '#multiple' => TRUE,
    '#options' => $view->fullcalendar_parse_fields(FALSE),
    '#default_value' => $view->options['fullcalendar_edit']['create_date_field'],
    '#dependency' => array(
      'edit-style-options-fullcalendar-create-click' => array(1),
      'edit-style-options-fullcalendar-create-select' => array(1),
    ),
  );

  $form['fullcalendar_edit']['create_revisions'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remove revisions info'),
    '#default_value' => $view->options['fullcalendar_edit']['create_revisions'],
    '#data_type' => 'bool',
  );

  $form['fullcalendar_edit']['create_authoring'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remove authoring info'),
    '#default_value' => $view->options['fullcalendar_edit']['create_authoring'],
    '#data_type' => 'bool',
  );

  $form['fullcalendar_edit']['create_publishing'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remove publishing info'),
    '#default_value' => $view->options['fullcalendar_edit']['create_publishing'],
    '#data_type' => 'bool',
  );

   // edit stuff

  $form['fullcalendar_edit']['edit_click'] = array(
    '#prefix' => t('Edit options:'),
    '#type' => 'checkbox',
    '#title' => t('Edit events when clicking on a event'),
    '#default_value' => $view->options['fullcalendar_edit']['edit_click'],
    '#data_type' => 'bool',
  );

  $form['fullcalendar_edit']['edit_revisions'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remove revisions info'),
    '#default_value' => $view->options['fullcalendar_edit']['edit_revisions'],
    '#data_type' => 'bool',
  );

  $form['fullcalendar_edit']['edit_authoring'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remove authoring info'),
    '#default_value' => $view->options['fullcalendar_edit']['edit_authoring'],
    '#data_type' => 'bool',
  );

  $form['fullcalendar_edit']['edit_publishing'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remove publishing info'),
    '#default_value' => $view->options['fullcalendar_edit']['edit_publishing'],
    '#data_type' => 'bool',
  );
}

/**
 * Implements hook_fullcalendar_options_process().
 */
function fullcalendar_edit_fullcalendar_options_process(&$variables, &$settings) {
  /*
  if ((empty($settings['fullcalendar_edit']['edit_click']))) {
    unset($settings['fullcalendar_edit']);
  }
  if ((empty($settings['fullcalendar_create']['click']) && empty($settings['fullcalendar_create']['select'])) || !node_access('create', $settings['fullcalendar_create']['node_type'])) {
    unset($settings['fullcalendar_create']);
  }
  */

  if ($settings['fullcalendar_edit'] || $settings['fullcalendar_create']){
    // not sure if need them here
    ctools_include('ajax');
    ctools_include('modal');
    ctools_modal_add_js();
  }else{
    unset($settings['fullcalendar_edit']);
  }
}
